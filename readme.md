# Python testing with Jupyter Notebook / NBgrader

Deze repo laat voor de drie Python modules uit de propedeuse (`inf1`, `inf2` en `inf3`) zien hoe toetsing via het NBgrader systeem zou kunnen plaatsvinden.  
Daarnaast bevat het een Jupyter notebook met docent instructies (onder `docent_instructies`) die laat zien hoe unit-like tests geschreven zouden kunnen worden om programmeervragen automatisch na te kunnen kijken.

Voor de NBgrader-specifieke zaken verwijs ik graag naar hun [website](https://nbgrader.readthedocs.io/en/stable/)

