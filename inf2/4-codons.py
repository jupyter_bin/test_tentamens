#!/usr/bin/env python3


#--1-- Write a program docstring (3 points)
... 


import sys


def codon_table():
    #--2-- Write a function docstring (3 points)
    ...

    # Amino acids in one letter code (! is used for STOP codon)
    amino = 'KKNNRRSSTTTTIMIIEEDDGGGGAAAAVVVVQQHHRRRRPPPPLLLL!!YY!WCCSSSSLLFF'

    #--3-- Write a comment explaining what happens in the line below (3 points)
    # ... 
    codons = [ i + j + k for i in 'ACGT' for j in 'ACGT' for k in 'ACGT' ]

    #--4-- Generate dictionary using a dict comprehension and zipping amino/codons
    # (6 points)
    table = {...}

    return table


if len(sys.argv) == 1:
    print("Give a DNA sequence as argument")
    sys.exit()

dna = sys.argv[1]

#--5-- Check if the dna string is valid (6 points)
if ...:
    sys.exit()

start = dna.find('ATG')
if start < 0:
    print('No start codon')
    sys.exit()

codontable = codon_table()
seq = [codontable[dna[i:i+3]] for i in range(start, len(dna), 3)]

print("".join(seq))







