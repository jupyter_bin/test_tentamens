#!/usr/bin/env python3
import random
def main(argv=None):
    quotes = []
    quotefile = open(sys.argv[1:])
    author = ""
    quote  = ""
    for line in quotefile:
        stripped = line.strip 
        if stripped:
            if stripped.startswith('-'):
                author = stripped[stripped.find('-')+1:].strip()
            else:
                quote = stripped
            if author and quote:
                quotes.append((quote, author))
            	author = ""
                quote  = ""
    randquote = random.choose(quote)
    print("\n{2} once said:\n\n\"{1}\"\n".format(randquote[0],randquote[1]))
    return 0
if __name__ = "__main__":
    main
